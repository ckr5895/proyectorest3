<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		
        <title>Inicio</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/main.css">
<script src="js/vendor/codigo.js"></script>
        <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
		
    </head>
    <body>

<div class="page-header" align="center"> 

<h1>API <small>Accidentes de tránsito terrestre en zonas urbanas y suburbanas</small></h1>
<br>

</div>

<div class="col-md-12" align=center>

<blockquote>
<p>La siguiente API le permite llamar a metodos REST  que son respondidas en sintaxis JSON o XML  según haya sido requerido.</p>
<br>
<p>La URL raíz de la API la encontramos en:</p>

<h4><a> www.dominio.com/public/api</a></h4>
</blockquote>

  </div>
<BR>
<div class="container">
<div class="row">
<div class="col-md-4">
<H3>Métodos API</H3>
<ul>
<li>	<a><strong>Entidades</strong></a></li>
	<ul>
	<li><a>Obtener entidades</a></li>
	<li><a>Obtener una entidad</a></li>
	<li><a>Agregar entidad</a></li>
	<li><a>Modificar datos de entidad</a></li>
	</ul>
	<br>
<li>	<a><strong>Municipio</strong></a></li>
	<ul>
	<li><a>Obtener municipios</a></li>
	<li><a>Obtener municipios de una entidad</a></li>
	<li><a>Agregar un municipio</a></li>
	<li><a>Modificar datos de municipio</a></li>
	</ul>
	<br>
<li>	<a><strong>Indicadores de accidentes</strong></a></li>
	<ul>
	<li><a>Obtener todas los indicadores</a></li>
	<li><a>Obtener datos de un indicador</a></li>
	<li><a>Agregar indicador</a></li>
	<li><a>Modificar datos de indicador</a></li>	
	</ul>
	<br>
<li>	<a><strong>Valores</strong></a>	</li>
	<ul>
	<li><a>Obtener conteo de accidentes</a></li>
	<li><a>Obtener conteo de un sólo año</a></li>
	<li><a>Obtener conteo de un  sólo indicador</a></li>
	<li><a>Agregar nuevo conteo</a></li>
	<li><a>Actualizar conteo de accidentes</a></li>
	</ul>
	<br>
</ul>
</div>
<div class="col-md-8" id="contenedor">
     
 </div>
</div>
</div>

	<script src="js/vendor/jquery-1.11.0.min.js"></script>
       <script src="js/vendor/bootstrap.min.js"></script>
       <script src="js/main.js"></script>
    </body>
</html>
